from os.path import realpath, dirname
from sys import path

# Set project path to python path
project_dir = dirname(realpath(__file__))
path.insert(0, "{}".format(project_dir))

from app import create_app
application = create_app(config_file='{}/app.cfg'.format(project_dir))
