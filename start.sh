#!/usr/bin/bash

# Create python virtual enviroment
if [ ! -d venv ]; then
  python3 -m venv venv
fi

# Load libraries
source venv/bin/activate
pip install -r requirements.txt  # Update python packages

# Start app
export FLASK_APP=app
export FLASK_ENV=development
flask run
