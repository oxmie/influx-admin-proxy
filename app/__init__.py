# Inspired by https://github.com/cburmeister/flask-bones
from flask import Flask
from flask_login import LoginManager, login_required

from app.config import DefaultConfig
from app.extensions import login_manager
from app.auth import auth
from app.content import content


def create_app(config_file='../app-dev.cfg') -> Flask:
    # Create and configure the app
    app = Flask(__name__)

    configure_app(app, config_file)

    register_extensions(app)
    register_blueprint(app)

    return app


def configure_app(app:Flask, config_file:str) -> None:
    # Apply default configuration
    app.config.from_object(DefaultConfig)

    # Override if config file exists, no erroy if does not exists
    # see: https://github.com/pallets/flask/blob/main/src/flask/config.py#L118
    app.config.from_pyfile(config_file, silent=False)


def register_extensions(app:Flask) -> None:
    login_manager.init_app(app)


def register_blueprint(app:Flask) -> None:
    app.register_blueprint(content)
    app.register_blueprint(auth)
