from flask import current_app
from flask_login import UserMixin

from app.extensions import login_manager
from app.influxdb import (
    get_databases, get_measurements, get_list_series, drop_measurement, delete_series
)

# Variable to save current active user objects
active_users = {}


class InfluxUser(UserMixin):
    def __init__(self, username: str, password: str) -> None:
        self.username = username
        self.password = password
        self.data_view = {}

    @property
    def id(self) -> str:
        return self.username

    def get_data_view(self) -> dict:
        return self.data_view


    def load_databases(self) -> None:
        # Update and return internal list
        databases = get_databases(self.username)

        # Update user data view
        self.data_view = { str(d): {} for d in databases }  # new data view

        current_app.logger.debug(
            "get_databases.user_data_view: {}".format(self.data_view))

        return databases

    def load_measurements(self, database: str) -> None:
        # Update and return internal list
        measurements = get_measurements(
            self.username,
            self.password,
            database
        )

        # Update user data view
        self.data_view = {  # new data view
            str(database): { str(m.get('name')): {} for m in measurements }
        }

        current_app.logger.debug(
            "get_measurements.user_data_view: {}".format(self.data_view))

        return measurements

    def load_series(self, database: str, measurement: str) -> None:
        # 'series_list' contains list of dict containing tags with values for a
        # serie and the name of the measurement at the beginning.
        # e.g. ['measurement,tag1=value1,tag2=value=2,...', '...']
        series_list = get_list_series(  # return list
            self.username,
            self.password,
            database,
            measurement
        )

        # Retrieve all tags used in series.
        # This means, there can be tags which are not used by one series, but
        # by some other series. This will be used within the table header at
        # the frondend site.
        tags = set()
        for s in series_list:
            # Skip measurement (1) and add only tag key (2) to list. Afterwards
            # add the new list of tags to 'tags'. There will be no duplicates
            # if tag already exists in tags, cause of python 'set' type.
            # (1):  s.split(',')[1:]
            # (2):  t.split('=')[0]
            tags.update([ str(t.split('=')[0]) for t in s.split(',')[1:] ])

        # Create list of series (without measurement name, see get_list_series())
        series = []
        for s in series_list:
            serie = {}
            for tag_value_str in s.split(',')[1:]:
                k,v = tag_value_str.split('=')  # tag format: 'key=value'
                serie.update({ str(k): str(v) })

            series.append(serie)

        # Update user data view
        self.data_view = {  # new data view
            str(database): {
                str(measurement): { 'tags': tags, 'series': series }
            }
        }

        current_app.logger.debug(
            "get_series.user_data_view: {}".format(self.data_view))

        return series

    def remove_measurements(self, database: str, measurements: list) -> bool:
        # perform deleten only, if database appears within user data view
        # prevent unauthorized database selection
        valid_request = False
        if database in self.data_view:
            valid_request = drop_measurement(database, measurements)

        current_app.logger.debug(
            "delete_measurements.measurements: {}".format(measurements))

        return valid_request

    def remove_series(self, database: str, measurement: str, series: list) -> bool:
        valid_request = False

        if database in self.data_view:
           valid_request = delete_series(database, measurement, series)

        current_app.logger.debug(
            "delete_series.series: {}".format(series))

        return valid_request


# Is called by @login_manager.user_loader
def get_user_by_id(user_id: str) -> InfluxUser:
    current_app.logger.debug(
        "login_manager.user_loader.active_users: {}".format(active_users))

    return active_users.get(user_id, None)
