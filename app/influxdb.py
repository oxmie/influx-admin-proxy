from flask import Flask, current_app, flash
from influxdb import InfluxDBClient
from influxdb.exceptions import InfluxDBClientError


# Create InfluxDBConnection to be used with python "with-statement"
class InfluxDBConnection(object):
    def __init__(self, username: str, password: str, database: str = None):
        self.host = current_app.config['INFLUXDB_HOST']
        self.port = current_app.config['INFLUXDB_PORT']
        self.ssl = current_app.config['INFLUXDB_SSL']
        self.verify_ssl = current_app.config['INFLUXDB_VERIFY_SSL']

        self.username = username
        self.password = password
        self.database = database

    def __enter__(self):
        # https://influxdb-python.readthedocs.io/en/latest/api-documentation.html#influxdbclient
        self.influxDBClient = InfluxDBClient(
            host = self.host,
            port = self.port,
            username = self.username,
            password = self.password,
            database = self.database,
            ssl = self.ssl,
            verify_ssl = self.verify_ssl
        )
        return self.influxDBClient

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.influxDBClient.close()


# Use get_list_database function to check if user exists
def is_valid_login(username: str, password: str) -> bool:
    valid = False
    with InfluxDBConnection(username, password) as connection:
        try:
            result = connection.get_list_database()
        except InfluxDBClientError as e:
            flash(e)
            current_app.logger.debug("is_valid_user.exception:\n{}".format(e))
        else:
            current_app.logger.debug(
                "is_valid_user.valid_login: (user: '{}', password: ******)".format(username))
            valid = True

    return valid


# Get list of databases available for user
def get_databases(username: str) -> list:
    db_list = []
    with InfluxDBConnection(
        current_app.config['INFLUXDB_ADMIN_USERNAME'],
        current_app.config['INFLUXDB_ADMIN_PASSWORD']) as connection:

        try:
            # Check if user is an admin user
            # Reason: If user is admin 'get_list_privileges' will return an
            # empty list, therefore this condition check is required.
            is_admin = False
            for user_item in connection.get_list_users():
                if( user_item['user'] == username ):
                    is_admin = user_item['admin']  # boolean
                    break

            if( is_admin ):
                # Load entrie database list
                db_list = [ i['name'] for i in connection.get_list_database() ]
            else:
                db_priv_list = connection.get_list_privileges(username)

                # Select only databases where user has all privileges. Only
                # 'WRITE' privilege is not enough, because 'load_measurements'
                # requires 'READ' privilege.
                for item in db_priv_list:
                    if item['privilege'] == 'ALL PRIVILEGES':
                        db_list.append(item['database'])

        except InfluxDBClientError as e:
            flash(e)
            current_app.logger.debug("get_databases.exception:\n{}".format(e))

    return db_list


# Get list of measurements for specific database
def get_measurements(username: str, password: str, database: str) -> list:
    measurement_list = []
    with InfluxDBConnection(username, password, database) as connection:
        try:
            measurement_list = connection.get_list_measurements()
        except InfluxDBClientError as e:
            flash(e)
            current_app.logger.debug("get_measurements.exception:\n{}".format(e))

    return measurement_list


# Get list of series for specific measurement
def get_list_series(
        username: str, password: str,
        database: str, measurement: str) -> list:
    series_list = []
    with InfluxDBConnection(username, password, database) as connection:
        try:
            # ['measurement,tag1=value1,tag2=value=2,...', '...']
            series_list = connection.get_list_series(measurement = measurement)
        except InfluxDBClientError as e:
            flash(e)
            current_app.logger.debug("get_series.exception:\n{}".format(e))

    return series_list


# Drop multiple measurements
def drop_measurement(database: str, measurements: list) -> bool:
    # perform deletion with database admin user
    with InfluxDBConnection(
        current_app.config['INFLUXDB_ADMIN_USERNAME'],
        current_app.config['INFLUXDB_ADMIN_PASSWORD'],
        database) as connection:

        for measurement in measurements:
            current_app.logger.debug('DROP MEASUREMENT "{}"'.format(measurement))
            connection.drop_measurement(measurement)  # perform deletion on database

    return True


# Delete series from measurement
def delete_series(database: str, measurement: str, series: list) -> bool:
    # perform deletion with database admin user
    with InfluxDBConnection(
        current_app.config['INFLUXDB_ADMIN_USERNAME'],
        current_app.config['INFLUXDB_ADMIN_PASSWORD'],
        database) as connection:

        for tags in series:
            current_app.logger.debug(
                'DELETE FROM "{}" WHERE {}'.format(measurement, tags))
            connection.delete_series(
                measurement = measurement,
                tags = tags
            )

    return True
