from markupsafe import escape

from flask import Blueprint, render_template, current_app, request, flash
from flask_login import login_required, current_user

content = Blueprint("content", __name__)


@content.route("/")
@content.route("/influxdb/")
@login_required
def index() -> str:
    current_app.logger.debug("route.index:")
    current_app.logger.debug("\tcurrent_user: '{}'".format(current_user))

    current_user.load_databases()
    return render_template('content/index.html',
        data = current_user.get_data_view()
    )


@content.route("/influxdb/<string:database>/", methods=['GET', 'POST'])
@login_required
def database(database: str) -> str:
    current_app.logger.debug("route.database:")
    current_app.logger.debug("\tcurrent_user: '{}'".format(current_user))
    current_app.logger.debug("\tdatabase: '{}'".format(escape(database)))

    selected_measurements = []
    if request.method == 'POST':
        selected_measurements = request.form.getlist('measurements')
        if request.form.get('confirm_deletion', False):

            # Perform deletion
            if current_user.remove_measurements(database, selected_measurements):
                flash("Following measurements were deleted: {}".format(
                    selected_measurements))
            else:
                flash("Invalid deletion request!")

            selected_measurements = []  # reset selected measurements
        elif request.form.get('abort_deletion', False):
            selected_measurements = []  # reset selected measurements

    current_user.load_measurements(escape(database))
    return render_template('content/database.html',
        database = database,
        data = current_user.get_data_view(),
        selected_measurements = selected_measurements
    )


@content.route("/influxdb/<string:database>/<string:measurement>/", methods=['GET', 'POST'])
@login_required
def measurement(database: str, measurement: str) -> str:
    current_app.logger.debug("route.measurement:")
    current_app.logger.debug("\tcurrent_user: '{}'".format(current_user))
    current_app.logger.debug("\tdatabase: '{}'".format(escape(database)))
    current_app.logger.debug("\tmeasurement: '{}'".format(escape(measurement)))

    selected_series_indices = []
    if request.method == 'POST':
        selected_series_indices = list(map(int, request.form.getlist('series')))

        if request.form.get('confirm_deletion', False):
            # Perform deletion
            series = [ current_user.get_data_view()
                        .get(database)
                        .get(measurement)
                        .get('series')[i]
                      for i in selected_series_indices ]
            if current_user.remove_series(database, measurement, series):
                flash("Series with following tag combination are deleted: {}".format(
                    series))
            else:
                flash("Invalid deletion request!")

            selected_series_indices = []  # reset selected series
        elif request.form.get('abort_deletion', False):
            selected_series_indices = []  # reset selected series

    current_user.load_series(escape(database), escape(measurement))
    return render_template('content/measurement.html',
        database = database,
        measurement = measurement,
        data = current_user.get_data_view(),
        selected_series_indices = selected_series_indices
    )
