import os
import secrets

class DefaultConfig(object):
    # Flask
    SECRET_KEY  = secrets.token_hex()
    PREFERRED_URL_SCHEME = 'https'

    # General InfluxDB configs
    INFLUXDB_HOST   = 'localhost'
    INFLUXDB_PORT   = 8086
    INFLUXDB_SSL    = False
    INFLUXDB_VERIFY_SSL = False

    # User specific settings
    INFLUXDB_ADMIN_USERNAME = 'admin'
    INFLUXDB_ADMIN_PASSWORD = ''
