from flask import (
    Blueprint, request, flash, current_app, redirect, url_for, render_template
)
from flask_login import login_required, login_user, current_user, logout_user
from flask_wtf import FlaskForm

from wtforms import StringField, PasswordField, SubmitField

from app.extensions import login_manager
from app.influxdb import is_valid_login
from app.user import InfluxUser, get_user_by_id, active_users

# Blueprint
auth = Blueprint("auth", __name__)

# LoginManager
@login_manager.user_loader
def load_user(user_id) -> InfluxUser:
    return get_user_by_id(user_id)

login_manager.login_view = 'auth.login'

# Routes
@auth.route("/login", methods=["GET", "POST"])
def login() -> str:
    form = LoginForm()
    if form.validate_on_submit():
        if is_valid_login(form.username.data, form.password.data):
            user = InfluxUser(
                username = form.username.data,
                password = form.password.data
            )

            # Remember and login user
            active_users[user.id] = user
            login_user(user)

            next = request.args.get('next')
            return redirect(next or url_for('content.index'))

        flash('Logged in failed.')

    current_app.logger.debug("auth.login - active_users: {}".format(active_users))

    return render_template('auth/login.html', form=form)


@auth.route("/logout")
@login_required
def logout() -> str:
    # Remove and logout current user
    active_users.pop(current_user.id)
    logout_user()

    flash('Logged out')
    return redirect(url_for('auth.login'))


# LoginManager formular fields
class LoginForm(FlaskForm):
    username = StringField('Username')
    password = PasswordField('Password')
    submit = SubmitField('Submit')
